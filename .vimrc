"set guioptions=em

"" Specify a directory for plugins
"" - For Neovim: ~/.local/share/nvim/plugged
"" - Avoid using standard Vim directory names like 'plugin'
"call plug#begin('~/.config/nvim/plugged')

"" On-demand loading
"Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

"Plug 'tpope/vim-surround'
"Plug 'tpope/vim-repeat'
"Plug 'tpope/vim-fugitive'
"Plug 'tpope/vim-commentary'

"Plug 'alvan/vim-closetag'

"Plug 'airblade/vim-gitgutter'
"Plug 'vim-airline/vim-airline'
"" Plug 'vim-syntastic/syntastic'
"Plug 'mileszs/ack.vim'
"Plug 'Valloric/MatchTagAlways'

""Plug 'mattn/vim-particle'

"" terminal inside vim
"" ConqueTerm* commands
"Plug 'ardagnir/conque-plus-plus'

"Plug 'kana/vim-textobj-user'
"Plug 'whatyouhide/vim-textobj-erb'
"Plug 'Townk/vim-autoclose'
"Plug 'bronson/vim-visual-star-search'
"Plug 'vim-scripts/SearchComplete'

""call Showcolornames(0)
""Gives errors if it tries to be loaded more than once
""Plug 'vim-scripts/colornames'

"" Initialize plugin system
"call plug#end()

"let g:syntastic_javascript_checkers = ['eslint']
"let g:sytastic_mode_map = {
"  \ "passive_filetpes": ["eruby"]
"  \}
"let g:airline#extensions#whitespace#enabled = 0
"let g:airline#extensions#syntastic#enabled = 0

""How can I open NERDTree automatically when vim starts up on opening a directory?
"autocmd StdinReadPre * let s:std_in=1
"autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif

"" Add eruby to 'Valloric/MatchTagAlways'
"let g:mta_filetypes = {
"  \ 'html' : 1,
"  \ 'xhtml' : 1,
"  \ 'xml' : 1,
"  \ 'jinja' : 1,
"  \ 'eruby' : 1
"\}

"map <C-\> :NERDTreeToggle<CR>

"" map Ctrl-s to save file
"map <C-s> :write<CR>
"imap <C-s> <Esc>:write<Cr>

"set mouse=ar
"set showcmd

"" Set path for easy searching in Rails
"set path=,,config/**,app/**,db/**,lib/**,sbm/**,ubm/**,vendor/assets/javascripts/angular/**

"set wildmenu
"set wildmode=longest:full,full,full
"set wildignorecase

"set number
"set relativenumber
"set nohlsearch
"set incsearch
"set ignorecase
"filetype on

"" indentation
"set tabstop=2
"set softtabstop=2
"set shiftwidth=0
"set expandtab
"set nowrap
"set smartindent

"set listchars=tab:>.,trail:.,extends:#,nbsp:.
"set list

"colorscheme elflord
"highlight Folded guibg=grey0 guifg=SkyBlue1
""highlight FoldColumn guibg=darkgrey guifg=white


"" filenames like *.xml, *.html, *.xhtml, ...
"" Then after you press <kbd>&gt;</kbd> in these files, this plugin will try to close the current tag.
""
"let g:closetag_filenames = '*.erb,*.html,*.xhtml,*.phtml'

"" filenames like *.xml, *.xhtml, ...
"" This will make the list of non closing tags self closing in the specified files.
""
"let g:closetag_xhtml_filenames = '*.xhtml,*.jsx'

"" integer value [0|1]
"" This will make the list of non closing tags case sensitive (e.g. `<Link>` will be closed while `<link>` won't.)
""
"let g:closetag_emptyTags_caseSensitive = 1

"" Shortcut for closing tags, default is '>'
""
"let g:closetag_shortcut = '>'

"" Add > at current position without closing the current tag, default is ''
""
"let g:closetag_close_shortcut = '<leader>>'
colorscheme desert
