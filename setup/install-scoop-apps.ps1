echo "scoop bucket add extras"
scoop bucket add extras
echo "scoop install extras/mc"
scoop install extras/mc

echo "scoop install extras/cpu-z"
scoop install extras/cpu-z

echo "scoop install extras/tabby"
scoop install extras/tabby

echo "scoop install main/sed"
scoop install main/sed

echo "scoop install main/mongodb-database-tools"
scoop install main/mongodb-database-tools

echo "scoop install spotube"
scoop install spotube

# pshazz slowed down the terminal and
# I didn't see much benefit
# echo "scoop install pshazz"
# scoop install pshazz

# It would be better to install the browsers
# or at least Brave using the vendor default
# installation method, b/c using the scoop
# installation method, when I open a link
# in another app, it opens a fresh Brave profile.
#
# I could look into if chocolatey would
# install the apps using the vendor default
# method. My guess would be it doesn't, but
# it might.
#
#echo "scoop install extras/brave"
#scoop install extras/brave
#echo "scoop install extras/vivaldi"
#scoop install extras/vivaldi

echo "scoop install extras/sourcetree"
scoop install extras/sourcetree

echo "sccop install extras/slack"
scoop install extras/slack

# I think signal auto-update installs a copy of signal into
# AppData\Local\Programs\signal-desktop
# don't install signal or other auto-updating programs with scoop
# echo "scoop install extras/signal"
# scoop install extras/signal
echo "scoop install extras/thunderbird"
scoop install extras/thunderbird
echo "scoop install extras/vcredist2022"
scoop install extras/vcredist2022
echo "scoop uninstall vcredist2022"
scoop uninstall vcredist2022
echo "scoop install main/neovim"
scoop install main/neovim
echo "scoop install extras/vscode"
scoop install extras/vscode
echo "scoop install main/go"
scoop install main/go
echo "scoop install main/make"
scoop install main/make
echo "scoop install extras/altsnap"
scoop install extras/altsnap
echo "scoop install extras/insomnia"
scoop install extras/insomnia

# gsudo is required to install PgAdmin4
echo "scoop install main/gsudo"
scoop install main/gsudo
echo "scoop bucket add nonportable"
scoop bucket add nonportable
echo "scoop install nonportable/pgadmin4-np"
scoop install nonportable/pgadmin4-np
# echo "scoop install extras/neovide"
# scoop install extras/neovide
# echo "scoop install main/lazydocker"
# scoop install main/lazydocker
