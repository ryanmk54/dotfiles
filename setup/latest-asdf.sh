#!/usr/bin/env bash

if [ ! -d "~/.asdf" ]; then
  echo "Installing asdf"
  sudo apt install curl git
  git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.9.0

  # source asdf so that the plugins can be added in the same script
  . $HOME/.asdf/asdf.sh

  echo ". $HOME/.asdf/asdf.sh" >> ~/.bashrc
  echo ". $HOME/.asdf/completions/asdf.bash" >> ~/.bashrc
fi

# Rust
echo "Installing asdf Rust plugin"
asdf plugin-add rust https://github.com/asdf-community/asdf-rust.git
asdf install rust latest
echo "rust $(asdf list rust | tail -n 1)" >> ~/.tool-versions
