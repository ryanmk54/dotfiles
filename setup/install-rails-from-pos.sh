#!/usr/bin/env bash

echo gem install rails -v $(grep -oP "[\'\"]rails.,[^0-9]*\K([0-9\.]+)" -- ~/src/pos/Gemfile)
gem install rails -v $(grep -oP "[\'\"]rails.,[^0-9]*\K([0-9\.]+)" -- ~/src/pos/Gemfile)

cd ~/src/pos
gem install bundler
bundle install
