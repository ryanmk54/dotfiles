alias ls='ls --color=auto'

export EDITOR=nvim

# Used for wsl. It doesn't work for native Ubuntu.
# ipconfig.exe shows windows ip config, but it requires windows interop
# grep
# * -A4 shows 4 lines after match
# cut
# * -d":" sets ":" as delimiter
# * -f 2 select only these fields. Probably only selects the part after the ":"
# tail -n1 select only the last line
# sed = stream editor. Looks like a substitution
# hostip=$(ipconfig.exe | grep 'vEthernet (WSL)' -A4 | cut -d":" -f 2 | tail -n1 | sed -e 's/\s*//g')
# export DISPLAY="${hostip}:0.0"
# export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | cut -d " " -f 2):0.0

eval `ssh-agent`
ssh-add ~/.ssh/git_prod

. $HOME/.asdf/asdf.sh
. $HOME/.asdf/completions/asdf.bash
