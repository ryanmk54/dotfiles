#!/usr/bin/env bash

# Set current directory to script directory
cd -P -- "$(dirname -- "$0")"

sudo apt update

# Install asdf version manager
# 
# curl and git are already installed on Ubuntu 20.04.2
# sudo apt install curl git
echo "Installing asdf"
if [ ! -d "~/.asdf" ]; then
  git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.8.1
fi

# Load asdf when starting bash
echo ". $HOME/.asdf/asdf.sh" >> ~/.bashrc

# Add asdf completions to bashrc
echo ". $HOME/.asdf/completions/asdf.bash" >> ~/.bashrc

# Source asdf to install plugins
. $HOME/.asdf/asdf.sh

# Read configuration from legacy version managers
echo "legacy_version_file = yes" >> ~/.asdfrc

ln -s $(readlink --canonicalize ../.tool-versions) ~/.tool-versions

# Install suggested build environment for ruby-build
# https://github.com/rbenv/ruby-build/wiki#suggested-build-environment
echo "Installing build environment for ruby-build"
sudo apt-get install -y autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm6 libgdbm-dev libdb-dev

echo "Installing postgresql and mysql client libraries"
sudo apt-get install -y libpq-dev libmysqlclient-dev

# TODO link $HOME/.default-gems

# I don't just install from the .tool-versions file
# b/c .tool-versions doesn't specify the url
# I'm using the command found on the plugin readme
echo "Installing asdf ruby plugin"
asdf plugin add ruby https://github.com/asdf-vm/asdf-ruby.git
echo "Installing asdf nodejs plugin"
asdf plugin add nodejs https://github.com/asdf-vm/asdf-nodejs.git
echo "Installing asdf yarn plugin"
asdf plugin add yarn
echo "Installing asdf python plugin"
asdf plugin add python
echo "Installing asdf neovim plugin"
asdf plugin add neovim

echo "Installing asdf plugin versions"
while read line; do
  plugin=""
  for word in $line
  do
    # if plugin variable is set, set the version
    if [ ! -z $plugin ]; then
      echo "Installing ${plugin} ${word}"
      asdf install $plugin $word
    else
    # else set the plugin variable
      plugin=$word
    fi
  done
done <../.tool-versions

# TODO test this to make sure it runs
# I might need to reload .bashrc first
# echo "Installing python neovim module"
# python2 -m pip install --user --upgrade pynvim
# python3 -m pip install --user --upgrade pynvim

echo "Installing tmuxinator"
sudo apt install -y tmux
gem install tmuxinator
sudo apt install -y wget
wget https://raw.githubusercontent.com/tmuxinator/tmuxinator/master/completion/tmuxinator.bash -O /etc/bash_completion.d/tmuxinator.bash

echo Linking tmuxinator/rails.yml
mkdir -p ~/.tmuxinator
ln -s $(readlink --canonicalize ../tmuxinator/rails.yml) ~/.tmuxinator/rails.yml
echo "" >> ~/.bashrc
echo "export XDG_CONFIG_HOME=$HOME/.config" >> ~/.bashrc

# Add ssh key
# -C = comment
# -f = file
# -N = new passphrase
ssh-keygen -C "ryan.m.kingston@esreco.net" -f ~/.ssh/ryan.m.kingston@esreco.net -N "" &&
  # Start ssh-agent
  eval $(ssh-agent) &&
  # adds ssh key to ssh-agent
  ssh-add ~/.ssh/ryan.m.kingston@esreco.net

# Install a clipboard manager
# https://askubuntu.com/questions/705620/xclip-vs-xsel
echo "Installing xclip"
sudo apt install xclip -y
echo "Restart your shell so that PATH changes take effect. Opening a new terminal tab will usually do it."

echo "Installing vim-packager"
git clone https://github.com/kristijanhusak/vim-packager ~/.config/nvim/pack/packager/opt/vim-packager

echo Linking init.vim
mkdir -p ~/.config/nvim
ln -s $(readlink --canonicalize ../init.vim) ~/.config/nvim/init.vim

echo Linking .tmux.conf
ln -s $(readlink --canonicalize ../.tmux.conf) ~/.tmux.conf

# TODO link extend_bashrc

# FIXME The coc extensions weren't installed after running :PackagerInstall
# Also figure out how to run this automatically
# Something like nvim --cmd
echo "Run :PackagerInstall after opening nvim to install plugins"
