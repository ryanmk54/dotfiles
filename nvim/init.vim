" Begin vim-packager
" https://github.com/kristijanhusak/vim-packager
if &compatible
  set nocompatible
endif

" https://github.com/dense-analysis/ale#5iii-how-can-i-use-ale-and-cocnvim-together
let g:ale_disable_lsp = 1

function! s:packager_init(packager) abort
  " Package manager using Neovim native package management
  call a:packager.add('kristijanhusak/vim-packager', { 'type': 'opt' })

  " Colorschemes picker
  call a:packager.add('booperlv/cyclecolo.lua')

  " Fuzzy File Finder
  call a:packager.add('junegunn/fzf', { 'do': './install --all && ln -s $(pwd) ~/.fzf'})
  call a:packager.add('junegunn/fzf.vim')

  " Conquer-of-Completion: VS-Code LSP (Language Server Protocol)
  call packager#add('neoclide/coc.nvim', { 'branch': 'release', 'do': function('InstallCoc') })
  " https://github.com/dense-analysis/ale#5iii-how-can-i-use-ale-and-cocnvim-together
  call packager#add('dense-analysis/ale')

  " File tree
  " I've been using coc-explorer instead of nerdtree
  " call a:packager.add('preservim/nerdtree')

  " Session management
  call a:packager.add('tpope/vim-obsession')

  " Buffers
  call a:packager.add('PhilRunninger/bufselect.vim')

  " One language plugin to rule them all
  " sheerun/vim-polyglot
  " uses HerringtonDarkholme/yats.vim for typescript highlighting
  " call a:packager.add('sheerun/vim-polyglot')
  " call a:packager.add('peitalin/vim-jsx-typescript')
  " call a:packager.add('leafgarland/typescript-vim')
  " call a:packager.add('HerringtonDarkholme/yats.vim')

  call a:packager.add('sjl/gundo.vim/')

  " NeovimQt Plugin for Gui commands
  call a:packager.add('equalsraf/neovim-gui-shim')

  " Narrow Region
  call packager#add('chrisbra/NrrwRgn')

  " Visual Ruby block selection
  call packager#add('kana/vim-textobj-user')
  call packager#add('nelstrom/vim-textobj-rubyblock')

  " Tagbar plugin
  call a:packager.add('liuchengxu/vista.vim')
  call packager#add('airblade/vim-gitgutter')

  call packager#add('tpope/vim-abolish')
  call packager#add('tpope/vim-capslock')
  call packager#add('tpope/vim-commentary')
  call packager#add('tpope/vim-repeat')
  call packager#add('tpope/vim-sensible')
  call packager#add('tpope/vim-surround')
  call packager#add('tpope/vim-fugitive')
  call packager#add('tpope/vim-rsi')

  call packager#add('itchyny/lightline.vim')

  call packager#add('MattesGroeger/vim-bookmarks')

  call packager#add('rescript-lang/vim-rescript')

  " Colorschemes
  call packager#add('patstockwell/vim-monokai-tasty')
endfunction

packadd vim-packager
call packager#setup(function('s:packager_init'))

function! InstallCoc(plugin) abort
  exe '!cd '.a:plugin.dir.' && yarn install'
  call coc#add_extension('coc-eslint', 'coc-explorer', 'coc-json', 'coc-phpls', 'coc-solargraph', 'coc-tsserver')
endfunction
" End vim-packager


" Begin suggestions from coc.nvim
" TextEdit might fail if hidden is not set.
" set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Give more space for displaying messages.
set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <c-cr> pumvisible() ? coc#_select_confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> <leader>gd :call CocAction('jumpDefinition', 'tabe')<cr>
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Remap <C-f> and <C-b> for scroll float windows/popups.
if has('nvim-0.4.0') || has('patch-8.2.0750')
  nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
endif

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of language server.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>
" End suggestions from coc.nvim

" Reset the statusline
" The coc.nvim suggestion doesnt' work.
" I'll have to look into it later
" set statusline=

let g:lightline = {
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'readonly', 'relativepath', 'modified', 'obsession', 'capslock' ] ]
      \ },
      \ 'inactive': {
      \   'left': [ ['relativepath', 'modified'] ]
      \ },
      \ 'component': {
      \   'capslock': '%{CapsLockStatusline()}',
      \   'obsession': '%{ObsessionStatus()}',
      \ },
      \ }

" The popup looks really bad in Neovide without the colors changed
" https://vi.stackexchange.com/a/12665
" set termguicolors
" highlight Pmenu ctermbg=DarkBlue guibg=DarkBlue
" highlight PmenuSel ctermbg=white guibg=white

" C-p is the Sublime shortcut to find files
" map <C-p> :FZF<CR>
map <C-p> :call fzf#run(fzf#wrap({'source': 'git ls-files'}))<CR>
" Find the current file in NerdTree
nnoremap <C-l> :NERDTreeFind<CR>
" Toggle NerdTree open and closed
nnoremap <C-\> :NERDTreeToggle<CR>

" leader keys
map <Leader>p :call fzf#run(fzf#wrap({'source': 'git ls-files'}))<CR>
map <Leader>b :Buffers<CR>
map <Leader>m :Marks<CR>
" map <Leader>l :NERDTreeFind<CR>
map <Leader>\ :NERDTreeMirror<CR>
map <Leader>e :CocCommand explorer<CR>

map <Leader>v :Vista coc<CR>
map <Leader>vv :Vista finder coc<CR>
" Map double click to fold or jump
autocmd FileType vista map <buffer> <silent> <2-LeftMouse> :<c-u>call vista#cursor#FoldOrJump()<CR>

" Prefix vim-bookmarks shortcuts with Leader
" https://github.com/MattesGroeger/vim-bookmarks#key-bindings
map <Leader>mm <Plug>BookmarkToggle
map <Leader>mi <Plug>BookmarkAnnotate
map <Leader>mn <Plug>BookmarkNext
map <Leader>mp <Plug>BookmarkPrev
map <Leader>ma <Plug>BookmarkShowAll
map <Leader>mc <Plug>BookmarkClear
map <Leader>mx <Plug>BookmarkClearAll

set mouse=a
set number

" spacing
" Some of files have been indented with tabs
" All the files I edit, indent with spaces
let g:polyglot_disabled = ['autoindent']
set expandtab
set shiftwidth=2
set tabstop=2

" monokai looks good with coc except for the tabs
" vim-monokai-tasty looks good and looks good with the tabs
set termguicolors
colorscheme vim-monokai-tasty

" https://vi.stackexchange.com/questions/14541/no-cursor-blinking-doesnt-work
set guicursor+=a:blinkon0

set wildmode=longest:full,full

" Use persistent history.
" https://advancedweb.hu/use-persistent-undo-in-vim-for-maximum-comfort/
if !isdirectory("/tmp/.vim-undo-dir")
    call mkdir("/tmp/.vim-undo-dir", "", 0700)
endif
if (!has("nvim"))
  set undodir=/tmp/.vim-undo-dir
end
set undofile

if has('nvim')
  lua require('cyclecolo').setup({})
	" These are the default options
	" lua require('cyclecolo').setup {
	" 	window_blend = 5, " Transparency of window, 0(none)-100(full).
	" 	window_breakpoint = 55, -- Determines the breakpoint where only the select window is shown, any number
	" 	filter_colorschemes = {}, -- Which colorschemes to not show in the selector, 'defaults' or {'table of strings'}
	" 	close_on_confirm = false, -- Whether or not to close the selector on confirm, true/false
	" 	hover_colors = false, -- Whether or not to set colorscheme to current one under the cursor
	" 	preview_text = 'lorem ipsum', -- String to set in the preview window 
	" 	preview_text_syntax = 'javascript', -- What syntax will be used in the preview window
	" 	--Alternatively, you can use autocmd for ColorScheme, though cyclecolo does not use this.
	" 	attach_events = {'dofile("/home/user/.config/nvim/lua/refreshhiglights.lua")'} --Lua functions to attach to colorscheme confirm
	" }
end

" TODO upgrade to packer.nvim

" https://www.chrisatmachine.com/Neovim/06-file-explorer/
" Explorer
let g:coc_explorer_global_presets = {
\   '.vim': {
\     'root-uri': '~/.vim',
\   },
\   'tab': {
\     'position': 'tab',
\     'quit-on-open': v:true,
\   },
\   'floating': {
\     'position': 'floating',
\     'open-action-strategy': 'sourceWindow',
\   },
\   'floatingTop': {
\     'position': 'floating',
\     'floating-position': 'center-top',
\     'open-action-strategy': 'sourceWindow',
\   },
\   'floatingLeftside': {
\     'position': 'floating',
\     'floating-position': 'left-center',
\     'floating-width': 50,
\     'open-action-strategy': 'sourceWindow',
\   },
\   'floatingRightside': {
\     'position': 'floating',
\     'floating-position': 'right-center',
\     'floating-width': 50,
\     'open-action-strategy': 'sourceWindow',
\   },
\   'simplify': {
\     'file-child-template': '[selection | clip | 1] [indent][icon | 1] [filename omitCenter 1]'
\   }
\ }

nmap <space>e :CocCommand explorer<CR>
nmap <space>f :CocCommand explorer --preset floating<CR>
autocmd BufEnter * if (winnr("$") == 1 && &filetype == 'coc-explorer') | q | endif
" End https://www.chrisatmachine.com/Neovim/06-file-explorer/
