-- https://jdhao.github.io/2021/07/11/from_vim_plug_to_packer/

-- vim.g.package_home = fn.stdpath("data") .. "/site/pack/packer/"
-- local packer_install_dir = vim.g.package_home .. "/opt/packer.nvim"

-- local plug_url_format = ""
-- if vim.g.is_linux then
--   plug_url_format = "https://hub.fastgit.xyz/%s"
-- else
--   plug_url_format = "https://github.com/%s"
-- end

-- local packer_repo = string.format(plug_url_format, "wbthomason/packer.nvim")
-- local install_cmd = string.format("10split |term git clone --depth=1 %s %s", packer_repo, packer_install_dir)

-- -- Auto-install packer in case it hasn't been installed.
-- if fn.glob(packer_install_dir) == "" then
--   vim.api.nvim_echo({ { "Installing packer.nvim", "Type" } }, true, {})
--   vim.cmd(install_cmd)
-- end

-- -- Load packer.nvim
-- vim.cmd("packadd packer.nvim")

-- -- the plugin install follows from here
-- -- ....

-- https://github.com/wbthomason/packer.nvim
-- This file can be loaded by calling `lua require('plugins')` from your init.vim

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

vim.cmd([[
  function! InstallCoc(plugin) abort
    exe '!cd '.a:plugin.dir.' && yarn install'
    call coc#add_extension('coc-eslint', 'coc-explorer', 'coc-json', 'coc-phpls', 'coc-solargraph', 'coc-tsserver')
  endfunction
]])

return require('packer').startup(function()
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

  use 'neoclide/coc.nvim', branch = 'release', run = vm.fn['InstallCoc']
end)
