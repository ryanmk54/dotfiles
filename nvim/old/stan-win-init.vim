set nocompatible
set hidden

" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.local/share/nvim/plugged')

" Make sure you use single quotes

" deoplete
" https://github.com/Shougo/deoplete.nvim
" requires python3
" :echo has("python3")
if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
  Plug 'Shougo/denite.nvim', { 'do': 'UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif
Plug 'carlitux/deoplete-ternjs', { 'do': 'yarn global add tern' }
let g:deoplete#enable_at_startup = 1

" neosnippet
" Plug 'Shougo/neosnippet.vim'
" Plug 'Shougo/neosnippet-snippets'

" Ordering deoplete and neosnippet first, then alphabetical, then colorschemes
" Plug '907th/vim-auto-save'
Plug 'airblade/vim-gitgutter'
Plug 'alvan/vim-closetag'
" Plug 'bling/vim-bufferline'
Plug 'bronson/vim-visual-star-search'
Plug 'chrisbra/NrrwRgn'
Plug 'doy/vim-diff-changes'
Plug 'google/vim-searchindex'
" Plug 'jlanzarotta/colorschemeexplorer'
Plug 'jlanzarotta/bufexplorer'
Plug 'jeetsukumaran/vim-indentwise'
" Plug 'lambdalisue/fila.vim'
Plug 'lambdalisue/gina.vim'
" Plug 'mizuchi/vim-ranger'
Plug 'mxw/vim-jsx'
Plug 'neomake/neomake'
Plug 'pangloss/vim-javascript'
" Plug 'rhysd/devdocs.vim'
Plug 'scrooloose/nerdtree', { 'on':  ['NERDTreeToggle', 'NERDTreeFind'] } " On-demand loading
Plug 'tmsvg/pear-tree'
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-dadbod'
Plug 'tpope/vim-dispatch'
" Plug 'tpope/vim-capslock'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-obsession'
" Plug 'tpope/vim-ragtag'
Plug 'tpope/vim-rails'
Plug 'tpope/vim-repeat'
" tpope/vim-rsi broke C-P, C-N command-line history
" Plug 'tpope/vim-rsi'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-surround'
" Plug 'tpope/vim-unimpaired'
Plug 'valloric/matchtagalways'
Plug 'vim-airline/vim-airline'
" Plug 'vim-scripts/CmdlineComplete'
" Plug 'vim-scripts/vim-json-line-format'
"Plug 'vim-scripts/SearchComplete'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
" Plug 'junegunn/vim-slash'

" typescript
" Plug 'HerringtonDarkholme/yats.vim'
" Plug 'mhartington/nvim-typescript', {'do': './install.sh'}

" colorschemes
" Plug 'altercation/vim-colors-solarized'
Plug 'ajh17/spacegray.vim'
" Plug 'flazz/vim-colorschemes'
Plug 'vim-scripts/Wombat'
Plug 'kamwitsta/mythos'
Plug 'wolf-dog/sceaduhelm.vim'
Plug 'danilo-augusto/vim-afterglow'
Plug 'kamwitsta/nordisk'
Plug 'felixhummel/setcolors.vim'

" Initialize plugin system
call plug#end()

" Setting termguicolors means terminal vim will look like gui vim
" as long as the terminal supports true colors 24-bit colors
if (has("termguicolors"))
  set termguicolors
endif
colorscheme elflord
let g:spacegray_underline_search = 1
" set tab bar color
" hi TabLine      guifg=#333 guibg=#222 gui=none ctermfg=254 ctermbg=238 cterm=none
" hi TabLineSel   guifg=#666 guibg=#222 gui=bold ctermfg=231 ctermbg=235 cterm=bold
" hi TabLineFill  guifg=#999 guibg=#222 gui=none ctermfg=254 ctermbg=238 cterm=none

if has('win32')
  let g:python3_host_prog = 'python'
end
" let g:python3_host_prog = 'python3'

" Plugin key-mappings for neosnippet
" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
" imap <C-k>     <Plug>(neosnippet_expand_or_jump)
" smap <C-k>     <Plug>(neosnippet_expand_or_jump)
" xmap <C-k>     <Plug>(neosnippet_expand_target)
" SuperTab like snippets behavior.
" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
"imap <expr><TAB>
" \ pumvisible() ? "\<C-n>" :
" \ neosnippet#expandable_or_jumpable() ?
" \    "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
" smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
" \ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
"" For conceal markers.
"" if has('conceal')
""   set conceallevel=2 concealcursor=niv
"" endif

" vim-closetag
let g:closetag_filenames = '*.html.erb,*.html,*.xhtml,*.phtml'

" nerdtree
map <C-\> :NERDTreeToggle<CR>
map <C-l> :NERDTreeFind<CR>

" fzf
map <C-p> :FZF<CR>
map \bf :Buffers<CR>

" vim-slash
" Places the current match at the center of the window.
" noremap <plug>(slash-after) zz
" " Blinking cursor after search using Vim 8 timer
" if has('timers')
"   " Blink 2 times with 50ms interval
"   noremap <expr> <plug>(slash-after) slash#blink(2, 50)
" endif


" neomake
" When writing a buffer (no delay).
"call neomake#configure#automake('w')
" When writing a buffer (no delay), and on normal mode changes (after 750ms).
"call neomake#configure#automake('nw', 750)
" When reading a buffer (after 1s), and when writing (no delay).
"call neomake#configure#automake('rw', 1000)
" Full config: when writing or reading a buffer, and on changes in insert and
" normal mode (after 1s; no delay when writing).
call neomake#configure#automake('nrwi', 500)

" matchtagalways filetypes
let g:mta_filetypes = {
      \ 'html' : 1,
      \ 'xhtml' : 1,
      \ 'xml' : 1,
      \ 'jinja' : 1,
      \ 'erb' : 1
      \ }

"https://github.com/tmsvg/pear-tree#dot-repeatability
let g:pear_tree_repeatable_expand = 0

let g:auto_save = 0 " enable AutoSave on Vim startup

" I turned off the auto-save notification
" because I did some substitutions and the auto-save notification
" took the place of the substitution notification
let g:auto_save_silent = 0 " do not display the auto-save notification

" Don't autosave gitcommit messages
" If they autosave it will commit if I quit with :q
" I want to abandon the commit when I quit vim without saving
autocmd Filetype gitcommit let g:auto_save = 0

" gitgutter options
" updatetime determines how long (in ms) how long the plugin
" will wait until after you stop typing before it updates the signs
set updatetime=400

" CmdLineComplete
cmap <c-y> <Plug>CmdlineCompleteBackward
cmap <c-e> <Plug>CmdlineCompleteForward

" Set IBeam shape in insert mode,
" underline shape in replace mode and block shape in normal mode.
" https://vim.fandom.com/wiki/Change_cursor_shape_in_different_modes
let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"

let g:sbm2 = "postgres://pgsql@aragorn.esreco.net/sbm2"
let g:purchasing = "postgres://pgsql@aragorn.esreco.net/purchasing"
let g:local_purchasing = "postgres://pgsql@localhost/purchasing"
let g:thorin = "postgres://pgsql@thorin.esreco.net/ofbiz"

" vim-autosave makes the swapfile redundant
set noswapfile

" https://gist.github.com/dergachev/d2c5eafe0c02938765dc
command! Diff execute 'w !git diff --no-index % -'

filetype plugin on
syntax on
set statusline=
set mouse=a
set list
set ignorecase smartcase
set expandtab shiftwidth=2 tabstop=2 softtabstop=2
set number relativenumber

" https://github.com/Kethku/neovide/issues/394
let s:fontsize = 14
function! GuiFont()
  " exe 'set guifont=FiraCode\ Nerd\ Font\ Mono:h' . s:fontsize
  exe 'set guifont=Consolas:h' . s:fontsize
endfunction
function! AdjustFontSize(amount)
  let s:fontsize = s:fontsize+a:amount
  call GuiFont()
endfunction
call GuiFont()

" Adjust font using the scroll wheel
" It doesn't seem to work
noremap <C-ScrollWheelUp> :call AdjustFontSize(1)<CR>
noremap <C-ScrollWheelDown> :call AdjustFontSize(-1)<CR>
inoremap <C-ScrollWheelUp> <Esc>:call AdjustFontSize(1)<CR>a
inoremap <C-ScrollWheelDown> <Esc>:call AdjustFontSize(-1)<CR>a

" Adjust font using the keyboard
noremap <silent> <C-=> :call AdjustFontSize(1)<CR>
noremap <silent> <C--> :call AdjustFontSize(-1)<CR>
