" Enable mouse support
set mouse=a

" Show statusline i.e. lightline
" https://github.com/itchyny/lightline.vim#introduction
set laststatus=2
" Show what is being typed
set showcmd
" It isn't necessary to show the mode since lightline shows it
set noshowmode
if !has('gui_running')
  set t_Co=256
endif

syntax on
filetype plugin indent on
" dart files look good with the industory colorscheme
colorscheme industry

set expandtab shiftwidth=2 tabstop=2 softtabstop=2
set number relativenumber

" Add fzf to runtimepath
set rtp+=~/.fzf

" NERDTree
map <C-\> :NERDTreeToggle<CR>
map <C-L> :NERDTreeFind<CR>
"
" " fzf
map <C-p> :FZF<CR>
map <leader>bf :Buffers<CR>"


let g:lightline = {
  \ 'inactive': {
  \   'left': [ ['filename', 'modified'] ]
  \ },
  \ 'active': {
  \   'left': [ [ 'mode', 'paste' ],
  \             [ 'readonly', 'filename', 'modified', 'Obsession'] ]
  \ },
  \ 'component': {
  \   'Obsession': '%{ObsessionStatus()}'
  \ },
  \ }
