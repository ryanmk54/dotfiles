shopt -s globstar

# For X11 forwarding
export DISPLAY=$(awk '/nameserver / {print $2; exit}' /etc/resolv.conf 2>/dev/null):0
export LIBGL_ALWAYS_INDIRECT=1

export FZF_DEFAULT_COMMAND='fdfind --type f --hidden --follow --exclude .git'

export PATH=/home/$USER/bin:$PATH
